from statsmodels.stats.proportion import proportion_confint
import scipy.stats as st
import numpy as np
from math import isnan
import psycopg2
import random
import pandas as pd
import openpyxl
from raritylevelstats.passwd_local import *

MAX_LOOPS = 100

CLASSES = [
{'name': 'RR', 'low': 0,'high': .01},
{'name': 'R', 'low': .01, 'high': .04},
{'name': 'C', 'low': .04, 'high': .16},
{'name': 'CC', 'low':.16, 'high': 1}]

def prop(count, nobs, alpha):
    return proportion_confint(count, nobs, alpha, 'wilson')

def class_prob(class_min, class_max, n, N):
    mean = n / N
    if class_min == 0.0:
        a = st.binom.cdf(-1, N, mean)
    else:
        a = st.binom.cdf(class_min*N, N, mean)
    b = st.binom.cdf(class_max*N, N, mean)
    return b-a

def get_class_summary(p):
    previous_class = False
    previous_p = 0.0
    add_next_class = False
    for b in CLASSES:
        a = b['name']
        if add_next_class:
            return '%s(%s)'%(previous_class,a)
        if p[a] >= .999:
            return '%s***'%a
        elif p[a] >= .99:
            return '%s**'%a
        elif p[a] >= .95:
            return '%s*'%a
        elif p[a] >= .9:
            return '%s'%a
        elif p[a] >= .75:
            return '%s?'%a
        elif previous_p + p[a] >= .95:
            return '%s-%s'%(previous_class, a)

        previous_class = a
        previous_p = p[a]

def prop_species(species_name, S, Q, N, ponderation=None):
    p = {}
    for the_class in CLASSES:
        prob = []
        for n in range(S, S+Q+1):
            prob.append(class_prob(the_class['low'], the_class['high'], n, N))
        if not ponderation:
            p[the_class["name"]] = sum(prob) / (Q+1)
    class_summary = get_class_summary(p)
    result = {
        'Species': species_name,
        'S': S,
        'Q': Q,
        'N': N
    }
    for x in p:
        result[x] = p[x]
    result['Ponderation'] = ponderation
    result['Class'] = class_summary

    return result

class db_connection:
    connection: object
    cursor: object

    def open(self):
       self.connection = psycopg2.connect(
            user=USER_DB,
            host=HOST_DB,
            port=PORT_DB,
            database=DATABASE,
            password=PASS_DB
            )
       self.cursor = self.connection.cursor()

    def get_db(self):
        return self.cursor

    def close(self):
        self.connection.commit()
        self.cursor.close()
        self.connection.close()

def create_db_data(db, geo_name, taxo_name, criterion):

    query = f"""
DROP TABLE IF EXISTS {SCHEMA}.{taxo_name}_rarity_map_sure_{geo_name} CASCADE;
CREATE TABLE {SCHEMA}.{taxo_name}_rarity_map_sure_{geo_name} AS
SELECT utm.id as mgrs1, syn.spec_id
FROM (SELECT oin.* FROM obsbe.waarneming oin LEFT JOIN obsbe.gebied g ON oin.id_gebied=g.geb_nr 
  WHERE {criterion}
  AND zeker='J'
) o
INNER JOIN {SCHEMA}.{taxo_name}_species_syn as syn ON o.id_vogel = syn.synonym_vogel_id 
	AND approx is false
LEFT JOIN gis.ifbl01x01 utm on ST_contains(utm.wkb_geometry, o.geom)
GROUP BY utm.id, syn.spec_id
;
GRANT ALL ON {SCHEMA}.{taxo_name}_rarity_map_sure_{geo_name} TO postgres WITH GRANT OPTION; 
"""
    db.execute(query)

    query=    f"""
DROP TABLE IF EXISTS {SCHEMA}.{taxo_name}_rarity_map_unsure1_{geo_name} CASCADE;
CREATE TABLE {SCHEMA}.{taxo_name}_rarity_map_unsure1_{geo_name} AS
SELECT utm.id as mgrs1, syn.spec_id
FROM (SELECT oin.* FROM obsbe.waarneming oin LEFT JOIN obsbe.gebied g ON oin.id_gebied=g.geb_nr 
  WHERE {criterion}
  AND zeker='J'
) o
INNER JOIN {SCHEMA}.{taxo_name}_species_syn as syn ON o.id_vogel = syn.synonym_vogel_id 
	AND approx is true
LEFT JOIN gis.ifbl01x01 utm on ST_contains(utm.wkb_geometry, o.geom)
GROUP BY utm.id, syn.spec_id
;
GRANT ALL ON {SCHEMA}.{taxo_name}_rarity_map_unsure1_{geo_name} TO postgres WITH GRANT OPTION
;"""
    db.execute(query)

    query=    f"""
DROP TABLE IF EXISTS {SCHEMA}.{taxo_name}_rarity_map_unsure2_{geo_name} CASCADE;
CREATE TABLE {SCHEMA}.{taxo_name}_rarity_map_unsure2_{geo_name} AS
SELECT utm.id as mgrs1, syn.spec_id
FROM (SELECT oin.* FROM obsbe.waarneming oin LEFT JOIN obsbe.gebied g ON oin.id_gebied=g.geb_nr 
WHERE {criterion}
  AND zeker='N'
) o
INNER JOIN {SCHEMA}.{taxo_name}_species_syn as syn ON o.id_vogel = syn.synonym_vogel_id 
	AND approx is false
LEFT JOIN gis.ifbl01x01 utm on ST_contains(utm.wkb_geometry, o.geom)
GROUP BY utm.id, syn.spec_id
;
GRANT ALL ON {SCHEMA}.{taxo_name}_rarity_map_unsure2_{geo_name} TO postgres WITH GRANT OPTION;
"""
    db.execute(query)

    query=    f"""
DROP TABLE IF EXISTS {SCHEMA}.{taxo_name}_rarity_map_{geo_name} CASCADE;
CREATE TABLE {SCHEMA}.{taxo_name}_rarity_map_{geo_name} AS
SELECT m.mgrs1, m.spec_id,
	case when s.spec_id is null then 0 else 1 end as sure, 
	case when u1.spec_id is null and u2.spec_id is null then 0 else 1 end as unsure
from
(SELECT mgrs1, spec_id from {SCHEMA}.{taxo_name}_rarity_map_sure_{geo_name} 
union SELECT mgrs1, spec_id from {SCHEMA}.{taxo_name}_rarity_map_unsure1_{geo_name} 
union SELECT mgrs1, spec_id from {SCHEMA}.{taxo_name}_rarity_map_unsure2_{geo_name}) as m
LEFT JOIN {SCHEMA}.{taxo_name}_rarity_map_sure_{geo_name} s 
    ON m.mgrs1=s.mgrs1 AND m.spec_id=s.spec_id
LEFT JOIN {SCHEMA}.{taxo_name}_rarity_map_unsure1_{geo_name} u1 
    ON m.mgrs1=u1.mgrs1 and m.spec_id=u1.spec_id
LEFT JOIN {SCHEMA}.{taxo_name}_rarity_map_unsure2_{geo_name} u2 
    ON m.mgrs1=u2.mgrs1 and m.spec_id=u2.spec_id
;
GRANT ALL ON {SCHEMA}.{taxo_name}_rarity_map_{geo_name} TO postgres WITH GRANT OPTION;
-- useful to get N
DROP TABLE IF EXISTS {SCHEMA}.{taxo_name}_rarity_geom_{geo_name};
CREATE TABLE {SCHEMA}.{taxo_name}_rarity_geom_{geo_name}  AS
select utm.wkb_geometry, c from
(select mgrs1, count(spec_id) as c
from {SCHEMA}.{taxo_name}_rarity_map_{geo_name} group by mgrs1) x
LEFT JOIN gis.ifbl01x01 utm ON utm.id=x.mgrs1;
GRANT ALL ON {SCHEMA}.{taxo_name}_rarity_geom_{geo_name} TO postgres WITH GRANT OPTION;
    """
    db.execute(query)

def get_geom_distribution(db, geo_name, big_box_table, big_box_geom_column, big_box_id_column,
                          taxo_name,
                          considerated_area_table, considerated_area_geom_column, considerated_area_condition=None
                         ):
    if not considerated_area_condition:
        considerated_area_condition = "1=1"

    query1 = f"""
    SELECT bbt.{big_box_id_column} as box,
     ST_Area(ST_Intersection(bbt.{big_box_geom_column}, considerated_area.{considerated_area_geom_column})) as part_area,
     ST_Area(bbt.{big_box_geom_column}) as total_area,
     ST_Intersection(bbt.{big_box_geom_column}, considerated_area.{considerated_area_geom_column}) as geom
    FROM {considerated_area_table} considerated_area 
    JOIN {big_box_table} bbt ON ST_Intersects(bbt.{big_box_geom_column},
                                                     considerated_area.{considerated_area_geom_column})
       WHERE {considerated_area_condition}
"""

#    print(query1)

    db.execute(query1)
    result1 = db.fetchall()
    geom_distrib = {}
    for a in result1:
        box = int(a[0])
#        print("area =",a[1], a[2])
        area_proportion = float(a[1]/a[2])
        geom_distrib[box] = {'sample': [], 'area_proportion': area_proportion}

    print(geom_distrib)

    query2 = f"""
    SELECT mgrs1 as sample, bbt.{big_box_id_column} as box
    FROM {SCHEMA}.{taxo_name}_rarity_map_{geo_name}
    LEFT JOIN gis.ifbl01x01 utm on utm.id=mgrs1
    LEFT JOIN {big_box_table} bbt ON ST_contains(bbt.{big_box_geom_column},utm.wkb_geometry)
    GROUP BY mgrs1, bbt.{big_box_id_column}
"""
    db.execute(query2)
    result2 = db.fetchall()
    for a in result2:
        sample = int(a[0])
        box = int(a[1])
        if box not in geom_distrib:
            assert False, "This should not occur! All boxes should have get an area_proportion before"
            geom_distrib[box] = {'sample': [], 'area_proportion': 1}
        geom_distrib[box]['sample'].append(sample)
    return geom_distrib

def get_random_sample(the_array, the_count):
    if the_count >= len(the_array):
        return the_array
    else:
        float_count = the_count - int(the_count)
        int_count = int(the_count)
        if random.random() * float_count > 0.5:
            int_count += 1
        return random.sample(the_array, int_count)

def get_sample_result(db, geom_distrib, geo_name, count_of_sample_per_big_box, taxo_name):
    # ici on va faire un croisement entre une table géométrique big_box_table
    # et les données qui sont dans {SCHEMA}.{taxo_name}_rarity_map_{geo_name}
    # pour obtenir un liste de carrés 1km par carré big_box_table
    # On va faire une sélection au hasard au sein de cette liste (on en prend count_of_sample_per_big_box)
    # pour chacune des grandes boîtes
    # Ensuite on fournit les données de l'ensemble des carrés 1km sélectionnés sur l'ensemble des grandes boîtes

    # bref 1°a on sélectionne des id de carrés (mgrs1) via sélection geom + hasard
    #      1°b on compte le nombre de ces carrés (=N qui sera retourné)
    #      2° on sélectionne les données en provenance de malaco_rarity_map_{geo_name}
    #         en veillant à ne pas fournir des données unsure là où on a du sure

    sample_count = {}
    random_geom_ids = []
    for line in geom_distrib:

        # establish count of sample proportionally to part of the big_box inside geom_distrib
        count_of_sample_per_present_big_box = count_of_sample_per_big_box * geom_distrib[line]['area_proportion']
        sample_count[line] = {'count': len(geom_distrib[line]['sample']),
                            'wanted_count': count_of_sample_per_big_box * geom_distrib[line]['area_proportion']
        }
        random_geom_ids += get_random_sample(geom_distrib[line]['sample'], count_of_sample_per_present_big_box)

    N = len(random_geom_ids)

    query = f"""
    SELECT spec_name as Species, coalesce(S,0), coalesce(Q,0) FROM
     {SCHEMA}.{taxo_name}_species sp 
    LEFT JOIN 
         (SELECT spec_id, SUM(sure) as S, SUM(unsure * (1-sure)) as Q
    FROM {SCHEMA}.{taxo_name}_rarity_map_{geo_name}
    WHERE mgrs1 in ({", ".join([str(s) for s in random_geom_ids])})
    GROUP BY spec_id) x
    on x.spec_id=sp.spec_id;
"""
    db.execute(query)
    result = db.fetchall()
    sample = []
    for line in result:
        sample.append({'Species': line[0],
                       'S': line[1],
                       'Q': line[2],
                       'N': N })
    print(sample)
    return sample, sample_count

def treat_one_sample(sample):
    result = {}
    for a in sample:
        species = a["Species"]
        S = a["S"]
        Q = a["Q"]
        N = a["N"]
        result[species] = prop_species(species, S, Q, N)
    return result

def get_mean_with_one_item_more(mean, p, n):
    assert mean
    for sp in mean:
        assert sp in mean
#        if sp not in mean:
#            mean[sp] = {'Species':sp}
        all_vars = ['S', 'Q', 'N'] + [x['name'] for x in CLASSES]
        for var in all_vars:
            if var == 'N' and sp in p:
                mean[sp]['N'] = p[sp]['N']
            else:
                assert var in mean[sp], "All species should already be in mean table"
                mean[sp][var] *= n
                if sp in p: # otherwise the species is absent from the sample, add 0
                    mean[sp][var] += p[sp][var]
                mean[sp][var] /= n+1
    return mean

def means_are_different(p, a):
    if not p:
        return True
    for sp in p:
        if p[sp]['Class'] != a[sp]['Class']:
            return True
    return False

def create_mean_with_list_of_species(db, geo_name, taxo_name):
   query = f"""SELECT spec_name as Species FROM (
       SELECT spec_id FROM {SCHEMA}.{taxo_name}_rarity_map_{geo_name}
           GROUP BY spec_id) x 
       LEFT JOIN {SCHEMA}.{taxo_name}_species sp on x.spec_id = sp.spec_id;
    """
   db.execute(query)
   result = db.fetchall()
   mean = {}
   for sps in result:
       sp = sps[0]
       print(sp)
       mean[sp] = {'Species': sp}
       all_vars = ['S', 'Q', 'N'] + [x['name'] for x in CLASSES]
       for var in all_vars:
           if var != 'N':
                mean[sp][var] = 0
           else:
                mean[sp]['N'] = -5000 # LOUIS
   return mean

def save_stats_for_province(prov_ids_in_obsbe, prov_name_without_accent,
                        big_box_table, big_box_geom_column, big_box_id_column,
                        count_of_sample_per_big_box,
                        user_ids,
                        taxo_name, taxo_id,
                        considerated_area_table, considerated_area_geom_column,
                        considerated_area_condition):
    criterion = get_criterion_for_malaco(user_ids, prov_ids_in_obsbe, taxo_id)
    save_stats(prov_name_without_accent,
                        big_box_table, big_box_geom_column, big_box_id_column,
                        count_of_sample_per_big_box,
                        criterion,
                        taxo_name,
                        considerated_area_table, considerated_area_geom_column,
                        considerated_area_condition)

def save_stats(geo_name,
                        big_box_table, big_box_geom_column, big_box_id_column,
                        count_of_sample_per_big_box,
                        criterion,
                        taxo_name,
                        considerated_area_table, considerated_area_geom_column,
                        considerated_area_condition):
    my_db = db_connection()
    my_db.open()
    db = my_db.get_db()

    create_db_data(db, geo_name, taxo_name, criterion)
    geom_distribution = get_geom_distribution(db, geo_name,
                                              big_box_table, big_box_geom_column, big_box_id_column,
                                              taxo_name,
                                              considerated_area_table, considerated_area_geom_column,
                                              considerated_area_condition)

    MAX_LOOPS = 1 # à remettre  LOUIS 50
#    MIN_LOOPS = 50
    previous_mean = False
    mean = create_mean_with_list_of_species(db, geo_name, taxo_name)
    n = 0
    while n < MAX_LOOPS:
        # and (n < MIN_LOOPS or means_are_different(previous_mean, mean)):
        s, sample_count = get_sample_result(db,
                geom_distribution, geo_name, count_of_sample_per_big_box, taxo_name)
        p = treat_one_sample(s)
        previous_mean = mean
        mean = get_mean_with_one_item_more(mean, p, n)
        n = n + 1
    for sp in mean:
        mean[sp]['Class'] = get_class_summary(mean[sp])

    mean_for_output = [mean[key] for key in sorted(mean)]

    pd_out_data = pd.DataFrame(data=mean_for_output)
    pd_out_quality = pd.DataFrame.from_dict(data=sample_count, orient='index')
    with pd.ExcelWriter(f"d:/!perso/{taxo_name}/raritylevel/rarity-level-{geo_name}-{n}.xlsx") as writer:
        pd_out_data.to_excel(writer, float_format='%.4f', index=False, sheet_name="stats")
        pd_out_quality.to_excel(writer, index=True, sheet_name="count sample")
    my_db.close()


def get_criterion_for_malaco(user_ids, prov_ids, taxo_id):
    user_list = ", ".join([str(s) for s in user_ids])
    prov_list = ", ".join([str(s) for s in prov_ids])
    criterion = f"""
g.prov_nr in ({prov_list})
AND ind_diergroep={taxo_id}
AND id_user in ({user_list})
AND datum >= '2010-01-01'"""
    return criterion

if False:
  save_stats_for_province([22], 'LIEGE',
            'gis.ifbl04x04', 'wkb_geometry', 'ogc_fid', 4,
            {44396, # Johann
                  43487 # Louis
                  },
            taxo_name="malaco",
            taxo_id=7,
            considerated_area_table='gis.provinces',
            considerated_area_geom_column='the_geom',
            considerated_area_condition="gid=7"
            )

if False:
 save_stats_for_province([22,23,24,25,26], 'Wallonie',
                'louis_tempo.ifbl10x10', 'wkb_geometry', 'id', 10,
                            {44396, # Johann
                                  43487, # Louis
                                  41859, # Tom
                                  44263, # Jelle
                                  40193, # Floris
                                  92585 #  Ward L
                                  },
                         taxo_name="malaco",
                         taxo_id=7,
                         considerated_area_table='gis.regions',
                         considerated_area_geom_column='geom',
                         considerated_area_condition="fid=2"
                         )

if False:
  save_stats_for_province([14,15,16,17,20], 'Vlaanderen',
                'louis_tempo.ifbl10x10', 'wkb_geometry', 'id', 10,
                            {44396, # Johann
                                  43487, # Louis
                                  41859, # Tom
                                  44263, # Jelle
                                  40193, # Floris
                                  92585 #  Ward L
                                  },
                          taxo_name="malaco",
                          taxo_id=7,
                          considerated_area_table='gis.regions',
                          considerated_area_geom_column='geom',
                          considerated_area_condition="fid=3"
                          )

if False:
  save_stats_for_province([19], 'Brussels',
                'ifbl01x01', 'wkb_geometry', 'ogc_fid', 1,
                            {44396, # Johann
                                  43487, # Louis
                                  41859, # Tom
                                  44263, # Jelle
                                  40193, # Floris
                                  92585 #  Ward L
                              },
                taxo_name="malaco",
                taxo_id=7,
                considerated_area_table = 'gis.provinces',
                considerated_area_geom_column = 'the_geom',
                considerated_area_condition = "gid=1"
  )

if True:
  save_stats_for_province([14,15,16,17,20,22,23,24,25,26,19], 'Belgium',
#                'louis_tempo.ifbl10x10', 'wkb_geometry', 'id', 10,
                'gis.ifbl32x20', 'wkb_geometry', 'ogc_fid', 10,
                            {44396, # Johann
                                  43487, # Louis
                                  41859, # Tom
                                  44263, # Jelle
                                  40193, # Floris
                                  92585 #  Ward L
                                  },
                taxo_name="malaco",
                taxo_id=7,
                considerated_area_table='gis.countries_l72',
                considerated_area_geom_column='geom',
                considerated_area_condition="name='Belgium'"
                        )
